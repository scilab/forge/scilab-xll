#include <xlwDotNet.h>
using namespace System;
using namespace Runtime::InteropServices;
using namespace xlwDotNet;

double //Evaluate command in Scilab software
 DLLEXPORT SLEvalString(const std::wstring&  command)
{
DOT_NET_EXCEL_BEGIN 
        return Example::ScilabFromExcel::SLEvalString(
                 gcnew String(command.c_str())
        );
DOT_NET_EXCEL_END
}
double //Create or overwrite Scilab matrix with data from Microsoft Excel worksheet
 DLLEXPORT SLPutMatrix(const std::wstring&  variable_name,const CellMatrix&  matrixvalues)
{
DOT_NET_EXCEL_BEGIN 
        return Example::ScilabFromExcel::SLPutMatrix(
                 gcnew String(variable_name.c_str()),
                gcnew xlwTypes::CellMatrix(IntPtr((void*)&matrixvalues))
        );
DOT_NET_EXCEL_END
}
CellMatrix //Write contents of Scilab matrix to Microsoft Excel worksheet
 DLLEXPORT SLGetMatrix(const std::wstring&  variable_name)
{
DOT_NET_EXCEL_BEGIN 
        return *(CellMatrix*)(xlwTypes::CellMatrix::getInner(Example::ScilabFromExcel::SLGetMatrix(
                 gcnew String(variable_name.c_str())
        )));
DOT_NET_EXCEL_END
}
