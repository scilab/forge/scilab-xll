#ifndef DLL_H
#define DLL_H

#include <xlwDotNet.h>

//<xlw:libraryname=Scilab_XLL


double //Evaluate command in Scilab software
SLEvalString(const std::wstring&  command // command to send to Scilab
);

double //Create or overwrite Scilab matrix with data from Microsoft Excel worksheet
SLPutMatrix(const std::wstring&  variable_name, // variable name
const CellMatrix&  matrixvalues // data
);

CellMatrix //Write contents of Scilab matrix to Microsoft Excel worksheet
SLGetMatrix(const std::wstring&  variable_name // variable name
);

#endif 
